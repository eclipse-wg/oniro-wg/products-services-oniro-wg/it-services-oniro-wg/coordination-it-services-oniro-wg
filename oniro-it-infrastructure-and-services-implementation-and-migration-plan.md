# Oniro IT Infrastructure and Services Implementation and Migration Plan v1.1.2

[[_TOC_]]

## 1 Goal

This document is an exercise to provide a realistic prioritization and a timeline for the execution and/or design of:
* The provisioning and, in some cases, running of new IT infrastructure and services for Oniro under EF infrastructure.
* Migration of existing infrastructure and services to become part of EF services for Oniro.
* The adaptation of Oniro pipelines, processes and workflows to the EF processes and services.

... by the Eclipse Foundation and Oniro project members.

The information provided is the result of an evaluation of the requirements, existing infrastructure, plans and demands from the Oniro project by different Eclipse Foundation teams. It relies on pre-existing agreements. Still, it is considered a living document and will be evolved throughout the year given that not in all cases EF have at this point all the information and details required to commit to concrete timelines. Also, the priorities and effort required to address those priorities are ever changing.  

## 2 Description of the infrastructure and services

This is a list of services that we at EF envision implementing or migrating for Oniro. The list is captured here for purposes of providing a longer-term roadmap for the various stakeholders.  They are divided into four groups:
* The first group (2.1.x) corresponds to those services that have gone through a prioritization process by the Oniro community and EF IT team.
* A second group of services (2.2.x) to implement or migrate to EF infrastructure, but that has not been prioritised or committed to yet.
* A third group of services (2.3.x) provided by default by EF that will be leveraged by the Oniro project.
* Maintenance: current and new services require maintenance effort that needs to be taken into account.

### 2.1 Prioritised services

The following list of IT related services have been identified by the various stakeholders as priority items, and are currently being actively worked on collectively by those stakeholders.  The ongoing status of each of these is being tracked in their respective issues; what is captured here is a high-level summary of each of them, their current status, and expected outcomes.  

#### 2.1.1 Delivery (Integration and Deployment) Pipelines
Eclipse Foundation has been traditionally offering Git, Gerrit and GitHub as an SCM together with a few other of its management services, like issues/tickets. The GitLab service was added in June 2020 for the entire Eclipse community in anticipation of the Oniro project’s migration to the EF. 

Migration consists of making Oniro project build/delivery processes functional, andto reduce the links with the existing OSTC infrastructure to the strict minimum.

New and existing rules and processes will be considered when defining the principles of Oniro pipelines.
* Security rules: use of containers with non-root privilege, management of secrets, whitelisting rules, ...
* Quotas: machine resources, disk space, etc…
* Rights: gitlab permission (developer, maintainer, etc.), …
* Secrets management
* Project configuration
* Others

In the current infrastructure Oniro projects provide and publish docker images in their own [gitlab registry](https://registry.ostc-eu.org/) like other artifacts published as “archive build artifacts” in the gitlab CI pipeline at Eclipse Foundation’s Gitlab. Artifacts management in the build process will be reviewed and it will become a specific part of the pipeline migration.

Pipeline migration will be focus on the following oniro projects:
* Oniro-core
* Oniro-blueprints
* Oniro-compliancetoolchain (limited to standard CI pipelines)

Oniro-compliancetoolchain uses two completely different kinds of CI pipelines: on the one hand, there are some standard pipelines for python testing (pytype, python unittests), REUSE lint checks, and creation and publication of docker images, which require just a standard infrastructure to execute (a standard gitlab runner and a docker registry) that can be substantially the same as the one needed for Oniro-core and Oniro-blueprints (such pipelines are defined [here](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends/-/blob/master/.gitlab-ci.yml)); on the other hand, there are custom CI pipelines, which do *not* run on the project's code repositories but on mirrors of the project repositories to be scanned (i.e. Oniro-core and possibly others) and which require a custom setup and a dedicated infrastructure. Only the first type may be migrated together with the pipelines of the other projects, while the second one will be migrated/adapted separately given its special nature. Please check the corresponding section on this document.

#### 2.1.2 Chat

The solution adopted in a previous study shows that the solution around matrix (protocol, SDK) is the most viable solution in terms of tools in accordance with the rules of use of the Eclipse Foundation and the needs of the Eclipse community as a whole.

In an early analysis, the Synapse server seems the preferred option as a backend and element-web as a web client. Third-parties will have the choice to directly use our implementation of matrix web client, or to federate with the EF matrix server.

All third parties chat software like mattermost or else proxies will not be implemented in the Eclipse Foundation infrastructure.

#### 2.1.3 License compliance toolchain

The Oniro project has invested a significant effort in creating a [continuous license compliance toolchain and associated process](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/img/oniro_pipelines_infra_diagram.md), integrated in gitlab pipelines. These tools and pipelines need to be migrated to the EF gitlab with little disruption on the existing service, because the current audit work in Oniro is designed as a continuous process that relies on the toolchain's seamless functioning. This is especially true in times around the Oniro release.

The IP and license compliance audit that will be done for the Oniro release scheduled in the 16th of December 2022 will be executed by the Eclipse Foundation IP Team with the existing tooling at EF following the existing EF IP Policy and IP Due Diligence process. The migration of the service described here does not affect the existing plans to perform such review by the EF IP team.

The following key actions are under scope of this plan:

* Definition of a detailed execution plan.
* While the EF IT team is expected to migrate the existing Oniro infrastructure, tools and data to the EF infrastructure, and maintain it indefinitely, careful planning and evaluation must take place before committing to services that will place an indefinite (and as of yet, unbudgeted) maintenance burden on the IT team. The Oniro project must be self-sustainable in the long term.
* The Oniro IP Team is expected to run the service as well as to provide expertise on requirements and potential solutions to specific infrastructure and provisioning issues. The EF IP Team has no plans to take over this responsibility.
* In addition, the Oniro IP Team will be responsible for the evaluation and tests of improvements to the existing set up as well as new tools that might be required or advised to be incorporated.
* The implementation of such improvements and provisioning of new tooling will require an additional evaluation by the EF IP Team.
   * This document only covers the migration of the existing toolchain from the current location (NOI Techpark and OSTC infrastructure) to EF one, not new tooling.

A specific migration plan will be developed by the EF IT team, the EF IP Team and the Oniro IP team, taking in consideration at least the following points:
* Availability of all teams to plan and execute the expected tasks.
* Current Oniro Platform Release Roadmap.
* Modifications on the existing toolchain and services due to infrastructure dependencies.
* Level of permissions and access rights required by the Oniro IP team to execute the license compliance service, compatible with EF policies in this regard.

The Oniro IP Team has put effort in providing a detailed overview of the existing IP/compliance toolchain and audit service to the EF. More specific details will be required to create together (EF and Oniro IP Team) a detailed execution plan. Such plan and execution will take in consideration the viability, maintenance costs, and associated ongoing effort associated with the infrastructure and provisioning of this service.

Based on the above, it is expected a tight collaboration between teams on:
* Planning of the migration and adapting to the EF infrastructure of the existing IP/license compliance toolchain currently in production for Oniro.
* Migration of the existing toolchain: tool set, pipelines, configurations and data, or altering the existing project processes to fit the existing toolchain.
* Implementation, integration with the existing toolchain and provisioning of new tools and services required in the future by Oniro.

#### 2.1.4 Signing service

Currently the Eclipse Foundation has a signing service. This service brings 2 core benefits:
* It provides a way for projects to sign software without the manual intervention of EF IT staff.
* It provides a way to safely use the EF certificate via a REST API and Maven plug-ins.

Current services allows two different methods for signing:
* Authenticode for Windows portable executables (i.e. .exe, .dll, …),
* Jar signing for Java platform, macOS bundle signing and notarization

Those services exist because the signature of binaries needs to be done with (paid) certificates issued by specific certificate authorities to be considered valid by the target platforms. The Eclipse Foundation cannot manage 1 certificate per target platform and per project, so those certificates must be shared. For security reasons, EF cannot share the certificates directly with projects.

If Oniro requires signing certificates for a different target platform than the above and that these target platforms require (paid) certificates from specific certificate authorities, then similar services will need to be developed. Such service might also be relevant for compatibility programs.

Alongside of the signing services, the Eclipse IT team maintain a Web of Trust of GPG keys and provide one GPG key per project part of this web of trust.

Associated with Gitlab, a similar service to the existing one will need to be developed and integrated so the project can use it without intervention of the EF IT team.

### 2.2 Non prioritised services

The following set of IT related services have been identified as very likely needed to support the longer term success of the Oniro project. However, these services are not currently identified as a priority and are not yet committed to.  The description of each is included to provide an overview of the status of each.    

#### 2.2.1 Connect Linaro’s LAVA with our own pipelines

The orchestration of the LAVA service provided by Linaro into Oniro pipelines will be part of the planned effort. Based on needs, given that this service is managed by Linaro and the hardware labs are distributed, that is, hosted by Oniro Members, the corresponding configurations in the Oniro pipelines will be applied and tested with the collaboration of the Linaro experts. Some additional learning process about how to apply and test these configurations will be carried on with the close support of the current service owner/experts at Oniro.

#### 2.2.2 Forum 

[Discourse](https://www.discourse.org/about), a modern open source forum-like tool, is requested by the Oniro project. As there are no plans to deploy such a service at the EF, the suggestion from EF is to use mailing lists,  Gitlab tickets and/or wikis to interact asynchronously with the wider community.

#### 2.2.3 Documentation

Oniro documentation is currently based on [readthedocs.org](https://docs.oniroproject.org/en/latest/) as a doc generation and a publish service. If required, EF provides a solution through Hugo documentation generation and deployment on a static server in EF infrastructure. Such generation can be smoothly integrated with the Oniro website.

Gitlab products offer another possibility with “gitlab pages” who can address documentation hosting for the Oniro project. This service is currently not implemented at EF. 

### 2.3 Services provided by EF that will be implemented for Oniro

The following list of IT related services have been identified by the various stakeholders as valuable to Oniro.  Generally, these services are standard offerings for projects, but are listed below here as it is expected the needs for Oniro are beyond these standard offerings.  

#### 2.3.1 GitLFS 

The [gitlfs storage service](https://git-lfs.github.com/) is present by default on gitlab instances. 

Similarly, quotas will have to be put in place in order to limit as much as possible the explosion of the volume due to the storage cost that can induce build artifacts such as those of docker images, or binaries.

#### 2.3.2 Back-ups and time-limited storage.

The backup policies applied to projects are those of the Eclipse Foundation when it comes to the gitlab instance.

The instances of the runners will be volatile in the cluster kubernetes okd environment. No persistence in this building context should be possible, except for exceptional reasons that must be first evaluated. (ex : cache management on runner).

#### 2.3.3 Download infrastructure

In addition to the build process, it is necessary to define where and how to host artifacts (content) that will be distributed as part of the Oniro releases.

Eclipse Foundation has a service for downloading artifacts. This existing service might or might not meet the Oniro project needs which will still need to be evaluated at some point in the future, and may be modified depending on priority and available resources.

### 2.4 Maintenance

While not an implementation task, ongoing maintenance should be considered a top priority service in the context of this document for at all times. One-time migration, implementation of new services, processes and data only accounts for a portion of the effort. Maintenance is expected to be factored into the equation of time available for new implementations.

Ongoing maintenance is a shared effort between Oniro teams and EF teams whenever possible, given that the proper functioning of the different services is a shared responsibility in many cases.

See also: https://wiki.eclipse.org/IT_SLA 


## 3 Prioritization

This is the 2022 priorities for the described services as well as some considerations.

| Priority | Service | Timeline (focus) | Considerations |
| :------: | :------: | :------: | ------ |
| 1 | Pipelines | July to November 2022 | The completion of the migration of the pipelines will move us away from the current intermediate state where key aspects of this feature/services are still hosted at OSTC infrastructure. |
| 2 | Chat | January - February 2023 | A significant part of the communication among community members and among ecosystem members takes place through a chat service that is today under OSTC. This service is essential for the development of Oniro and the management of the project. In order to guarantee the Open Governance and specially the vendor neutral nature of the project, this service should be moved under EF. Participants in communications must be covered by the EF legal framework. |
| 3 | License compliance toolchain | March - May 2023 (*) | The migration of this service will require the creation of a detailed joined plan which will be the initial step. There are strong dependencies in this effort between both involved teams which will influence that detailed plan, specially in November. |
| 4 | Signing service | June 2023 (**) | Oniro project must evaluate the existing EF signing process and  communicate its signing requirements should the existing service be inadequate. |

Ongoing Maintenance is a priority at all times for every service migrated or implemented from scratch for Oniro.

Once these services are implemented, we will open a new evaluation period so we can reach a similar consensus about the rest of the proposed services or new ones that might arise.
 
**License compliance toolchain (*)**

After going through a detailed walk-through given to EF staff by the Oniro IP/compliance team, the effort to migrate the existing toolchain needs to be scoped and the execution planned in detail. Currently, our assumption is that the migration, testing, put in production and switch-off of the existing solution will not be done completely by the end of October. EF acknowledges that such a conclusion represents a challenge for the Oniro IP/compliance team given the release timeline. It is therefore assumed that the existing IP toolchain and the Oniro IP team will be used to complete the required clearance work in support of the Oniro v2.0 release planned for December.

The planning of this migration will contemplate this dependency and challenge to make the migration compatible with the responsibilities that the Oniro IP Team have in the coming Oniro release. In the worst case scenario, the remaining work that depends on the active participation of the Oniro IP/Compliance team will be scheduled for the beginning of 2023, based on a common agreement.
 
**Signing Service (**)**

Based on the decisions taken on the planning and execution of the IP/license compliance service, the planning of the execution of this service might be altered.

## 4 Stakeholders

This is the people directly involved in the execution of the described activities, acting as points of contact and subject experts.

* EF staff
	* Sébastien Heurtematte sebastien.heurtematte@eclipse-foundation.org
    * Fred Gurr frederic.gurr@eclipse-foundation.org
    * Denis Roy denis.roy@eclipse-foundation.org
    * Matthew Ward webmaster@eclipse-foundation.org
    * Jakub Mazanek webmaster@eclipse-foundation.org
    * Wayne Beaton wayne.beaton@eclipse-foundation.org
* Oniro teams
  * Oniro IT team
    * Pawel Stankiewicz: pipelines and general coordination pawel.stankiewicz@huawei.com
    * Stevan stevan.radakovic@linaro.org
    * Pavel Zhukov pavel.zhukov@huawei.com
    * Andrei Gherzan andrei.gherzan@huawei.com
  * Oniro IP team 
	  * Luca Miotto: general coordination l.miotto@noi.bz.it 
	  * Oniro Legal team
		  * Carlo Piana piana@array.eu
		  * Alberto Pianon pianon@array.eu
	  * Oniro Audit team
		  * Rahul Mohan G. rahul.mohan.g@huawei-partners.com
		  * Vaishali Avhad vaishali_avhad@yahoo.com
	  * Oniro IP Toolchain Development team 
		  * Alberto Pianon pianon@array.eu
		  * Peter Moser p.moser@noi.bz.it
		  * Martin Rabanser martin.rabanser@rmb-consulting.tech
		  * Chris Mayr chris@1006.org

Please contact Agustín Benito Bethencourt for any questions about this document.

## 5 Communication channels and documentation 

Communication channels and documentation that we will use in this area of this work: 
* Public requirements documentation: [it-services-oniro-wg](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg)
  * `coordination-it-services-oniro-wg`: aim to define and plan infrastructure and service migration (contains meeting reports with IT Teams)
     * MoMs https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/tree/main/mom-coordination-it-sservices-oniro-wg
     * This document is hosted in [this repository](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-services-plan.md). Feel free to improve it by providing MRs. 
   * `pipelines-architecture-oniro-wg`: requirements about Oniro infrastructure to prepare migration in EF infrastructure
	   * [MoMs](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/tree/main/mom-coordination) about this topic
	   * [oniro-pipelines-configuration.adoc](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/oniro-pipelines-configuration.adoc) This document aims at a better understanding of the Oniro pipelines configuration.
	   * [oniro-pipelines-infrastructure.adoc](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/oniro-pipelines-infrastructure.adoc) This document aims at a better understanding of the Oniro pipelines infrastructure.
	   * [oniro-pipelines-migration-strategy.adoc](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/oniro-pipelines-migration-strategy.adoc) This document aims at a better understanding and to determine of Oniro pipelines migration perimeter and constraint.
  * More requirements project to come with `License Compliance Toolchain`, `Signing Service`, ... 
* Public service implementation documentation 
  * Each service at EF that needs specific development is hosted in the [eclipsefdn/it/releng](https://gitlab.eclipse.org/eclipsefdn/it/releng) group in EF Gitlab.
* Private documentation will be hosted internally at EF.
* The Oniro website:
   * oniroproject-org [repository](https://gitlab.eclipse.org/eclipsefdn/it/websites/oniroproject.org/-/tree/main)
	* The design work that should lead to an execution plan can be tracked in the ticket [#26](https://gitlab.eclipse.org/eclipsefdn/it/websites/oniroproject.org/-/issues/26)

Synchronous communication (Chat):
* #oniroproject IRC channel at libera.chat 
* Once matrix is set up, new channels as well as usage guidelines will be defined. 

Announcements and discussions: 
* Default communication channel and everything related with community/projects: oniro-dev mailing list
* Conversations and announcement of interest to our Oniro ecosystem: oniro-wg mailing list

Requests and tickets:
* [Help desk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues?sort=priority&state=opened) to interact with the EF IT and releng team by default.
* Management and coordination tasks related with the planning of the migration of described services can be filled out in the [it-ervices-oniro-wg](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg) subgroup. If tickets in this group belongs to Help Desk, they will be moved there.

This section of the document is extensible. Please provide additional or refreshed links through MRs.

## 6 Timeline

In addition with the high level timeline referenced above (table), the activity developed by the releng team can be followed through this more detailed working plan, that is updated regularly:
* [Current timeline for service migration](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/roadmap?state=all&sort=start_date_asc&layout=WEEKS&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL). Currently only the services that have been scoped are reflected here. Following a just-in-time approach, such planning can be adapted.
   * Each service will be described as an Epic.
   * Each outstanding task will be [described as a ticket](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/issues?sort=priority&state=opened) linked to the corresponding epic.
   * As usual at Oniro WG Gitlab, a [priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852) and a [workflow board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/2466) will provide a better understanding of progress.
      * Given that the chat service will be rolled out across the entire Foundation, most activities are not Oniro specific but EF. Therefor those non-Oniro specific activities (most of them) can be tracked in a specific pair of Boards: [Chat service priority board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/2687) and [Chat service workflow board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/2111)

This plan takes advantage of the following Gitlab feature, [Gitlab roadmap](https://docs.gitlab.com/ee/user/group/roadmap/), which is used within the `/it-services-oniro-wg` subgroup to present working progress, release date, ... 

New epics correspoding to new services and tasks associated to them will be created on a continuous basis, following the just-in-time principle. 

## 7 Funding

The effort required to execute the proposed activities during 2022 are based on the approved [Oniro Budget 2022](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf), created in support of our [Oniro Program Plan 2022](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf).

Any change in such funding will affect this effort, that would need to be adjusted. Maintenance costs associated with the migrated services or those newly implemented have been and will remain in the future an element of careful consideration by EF.
 
## 8 Links and references

EF Gitlab specifications:
* Some of the specifications related with Gitlab are part of the EF [handbook](https://www.eclipse.org/projects/handbook/#resources-gitlab).
* Some others are defined at the Eclipse Foundation Wiki with Category [releng](https://wiki.eclipse.org/Category:Releng)

IP/compliance Reference Materials:
* [High level introduction on IP compliance in the Oniro project (WIP)](https://git.ostc-eu.org/oss-compliance/wip/oniro-design/-/blob/main/what_Oniro_is_all_about.md)
* FOSSology: https://git.ostc-eu.org/oss-compliance/fossology
* Scancode: https://github.com/nexB/scancode-toolkit (directly integrated in compliance toolchain docker image: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/-/tree/master/infrastructure/docker)
* OSS compliance toolchain: https://git.ostc-eu.org/oss-compliance/toolchain
* OSS compliance pipelines: https://git.ostc-eu.org/oss-compliance/pipelines
* Oniro Bill of Materials: https://git.ostc-eu.org/oss-compliance/sbom
  * audit process management: https://git.ostc-eu.org/oss-compliance/sbom/private_bom/-/issues

Links related with HelpDesk and other IT activities can be found at [Oniro Join Us and Contribute](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Join_Oniro#links-and-references) wiki page.

This section of the document is extensible. Please provide additional or refreshed links through MRs.

## 9 Nomenclature

* EF: Eclipse Foundation
* IT: information technology
* PgM: Program Manager
* releng: Release Engineering
* IP Intellectual Property
* OSS Open Source Software

## 10 Changelog

Relevant revisions of the document:
* 2023-01-25 Version 1.1.2 New dates for the timeline
* 2023-01-23 Version 1.1.1 Link to chat service implementation boards added. Typos fixed
* 2022-09-27 Version 1.1.0 Several rounds of small fixes. Additional links added. Timeline adapted. 
* 2022-08-24 Version 1.0 First version of the document.
