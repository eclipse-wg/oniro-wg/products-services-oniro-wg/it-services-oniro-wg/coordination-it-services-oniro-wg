# Oniro IT infrastructure and services coordination meeting 2022wk40

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)

## Agenda

* Actions from last week 
* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 

## Migration Pipeline issues 

Oniro configuration done!

See configuration and global performance: 

(Oniro Migration: Global pipeline performance)[https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/27]


Creation of two runners for Oniro: 
* runner at group level  
* runner dedicated to oniro project 

Question: 

* To whom issue (review) and MR should be assigned?
  * Assigned to Andrei and/or Stefan.


### Oniro Migration : build docker image with kaniko! (root user at EF) (#10) (OPEN) (Block) (PRIORITY IMPORTANT)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/10


### Oniro Migration: initial PVC (#18) (OPEN) (Review) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/18

Provide cache directory structure creation for PVC in gitlab-ci.


### Oniro Migration: error dialing backend: remote error: tls: internal error. (#16) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/16

EF infrastructure pb.
Try workaround with CI_SERVER_TLS_CA_FILE to implements in runner configuration.


### Oniro Migration: host contamination (#21) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/21

need to test configuration about bitbake warning

### Oniro Migration: bitbake shared-state-cache working and strategy (#29) (OPEN)  (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/29

Investigation around bitbake cache management:

How it works?
How it grows up?
What is the Oniro team strategy about invalidating cache, ttl, metrics on sizing evolution, ...?

NOTE: Each pipeline build grows cache by 1G of storage.

Reply from @Pawel

## Eddie migration

### Eddie migration: access denied (#34) (OPEN)  (PRIORITY IMPORTANT)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/34

## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D

### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

No change!!

### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

No change!!


### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

No change!


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602



## Decisions

All requests will be assigned to Andrei and/or Stefan.


## Actions

Sebastien: 
* continue project migration

