# Oniro IT infrastructure and services coordination meeting 2022wk41

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)

## Agenda

* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 

## Migration Pipeline  


Gitlab runner organization modification: 
* runner dedicated to oniro project move to group (shared with meta-openharmony) 


## Oniro migration

2 MR: 
* [Oniro Pipeline migration: migration to dockerhub (Ready to merge)](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/339) 
* [Migration EF runner (Draft) waiting on answer](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/332)
  => [Oniro migration: zephir-*-clang, linux-seco-*, freertos-*-clang build job failed](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/36)
  

Issue assign to Andrei and Stefan.
Comment in the MR directly.

[Oniro Migration: bitbake shared-state-cache working and strategy](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/29)

what about pawel scripts?

## Hlaas migration

(Migration EF runner)[https://gitlab.eclipse.org/eclipse/oniro-core/HLaaS/-/merge_requests/39]

Ready to merge!


## meta-openharmony migration

[Migration EF runner (draft))](https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony/-/merge_requests/86)

NOTE: Migration needs to be coordinate. => change CI_REGISTRY_* values
see: [eclipse/oniro-core/meta-openharmony!86](https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony/-/merge_requests/86#note_1026196) 


[meta-openharmony Migration: runner in china area](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/35)

=> any news?


[meta-openharmony migration: SSL verification failure](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/39)

workflow: in progress

[meta-openharmony migration: Reusing repo mirror failed!](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/46)

workflow: in progress

## sysota migration

[EF pipeline migration: squashfs-tools should be provided by an image](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/25)

=> create a new docker image with squashfs
workflow:in Progress

[EF pipeline migration: Arch target on job go-test](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/24)
workflow:done

[sysota migration: permission denied on moving go cache](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/40)

=>GOPATH: $CI_PROJECT_DIR.tmp/go
workflow:done

[sysota migration: spread-qemu: [impish, ubuntu, 21.10] failed!](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/45)

=>propose to remove ubuntu 21.10 from spread matrix job.
workflow:done

[Sysota migration: cspell new version](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/42)

New spread image in oniro project
=> allow_failure: true
workflow:done

[sysota migration: go-fmt job formatting failed](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/43)

=> format two files for go fmt.
workflow:done

[EF pipeline migration: Arch target on job cross-build](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/26)
=> missreading
workflow:done

## eddie

[EF pipeline migration: ci job test failed with](https://gitlab.eclipse.org/eclipse/oniro-core/eddie/-/issues/17)
=> waiting answer

```
    request.path = ".well-known/core";
```

[EF pipeline migration: ci job eddie-zephir failed failed due to permission denied](https://gitlab.eclipse.org/eclipse/oniro-core/eddie/-/issues/18)

Comment from Andrei: We can drop that specific configuration for a performance impact. We would need to test to see what that amounts to. 

## meta-zephyr migration

Bloc by oniro migration

```
    - '/.oniro-ci/dco.yaml'
    - '/.oniro-ci/reuse.yaml'
    - '/.oniro-ci/build-generic.yaml'
    - '/.oniro-ci/test-generic.yaml'
    - '/.oniro-ci/machines-and-flavours.yaml'

```

=> will be test with specific branch if it take too logn to merge

## docs

Bloc by oniro migration

```
   file:
    - .oniro-ci/dco.yaml
    - .oniro-ci/reuse.yaml
```

=> will be test with specific branch if it take too logn to merge

## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D


### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

Awaiting feedback!


### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

No change!


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602



## Decisions



## Actions

