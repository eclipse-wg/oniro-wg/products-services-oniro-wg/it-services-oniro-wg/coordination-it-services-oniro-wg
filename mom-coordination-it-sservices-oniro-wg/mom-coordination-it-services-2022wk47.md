# Oniro IT infrastructure and services coordination meeting 2022wk47

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)

## Agenda

The structure (agenda) :
* Ef-focus - Oniro PgM 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## Ef-focus - Oniro

### oniro compliance toolchain - Starting migrating 

First step done!

All projects migrated: 
* tinfoilhat
* aliens4friends
* pipelines
* dashboard
* docs

Few minor issues! dockerhub namespace.

### DNS issue for resolving artifact from github

EF infrastructure while building docker images encountered problem -i/o timeout. with UDP request and coredns (internal DNS implementation in OKD).

Tests must be done on a more recent version of OKD. 4.10

### Chat service

start working on implentation of Matrix synapse as server and elementweb as web client.

Priority 2 from migration plan.
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md#3-prioritization

Board consulting:
https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/

## Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)


### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

Comments were in `pending mode` on gitlab.com for 2 weeks. thread validate!
Task: send a email to PMC 

## Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

=> waiting on feedback from Stefan


### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

Need feedback!

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project


## Prospects pipeline

### Oniro release

Release this week!
Tag signing will be ask after the release via an open issue in helpdesk.


## Questions/decisions

Specific topic about legal artifacts provided by CI so downloadable in public area vs applying a restricted visibility.
An open issue in helpdesk must be open.

[[oniro] Disable public access for artifacts in the oniro-core group](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2284)

