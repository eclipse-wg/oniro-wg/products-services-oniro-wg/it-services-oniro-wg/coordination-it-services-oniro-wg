# Oniro IT infrastructure and services coordination meeting 2022wk49

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)

## Agenda

The structure (agenda) :
* Focus EF - Oniro 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## 1 - Focus EF - Oniro

### release 2.0.0

[[oniro] GPG signed release tags for v2.0.0](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2349#note_1050951)
[ Add 2.0.0 release artefacts ](https://gitlab.eclipse.org/eclipsefdn/it/websites/oniroproject.org/-/merge_requests/28)
[[Oniro Compliancetoolchain] Prevent artifacts of specific jobs from being deleted](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2312)

Retag all version 2.0.0 with private gpg key from eclipse
Publish public key and artifacts to `download.eclipse.org`: https://download.eclipse.org/oniro-core/releases/2.0.0/

NOTE: `meta-ledge-secure` and `meta-ts` projects default branch is not `oniro/kirkstone` and there are not protected.

see comment: https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2349#note_1050928

### Tag process

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

Proposition jobs for tagging version: 
* Create release branch from default branch (kirkstone) (pattern use releases/v2.0.0)
* Trigger pipeline from Release branch
* Tag current branch and sign tag with private key 


### Publish process

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

Propose job to publish artifacts to download.eclipse.org.

What about checksum and signature files?



### cleanup jobs artifacts from Oniro and forks Oniro project

[[oniro] Disable public access for artifacts in the oniro-core group](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2284)

Cleaning in process. Dealing with gitlab api throttling. 

### HELPDESK Mirror repositories doesn't work

[[oniro] Mirror repositories doesn't work](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581)

[can you confirm Oniro interest for this feature?](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581#note_1049520)

### DNS issue for resolving artifact from github

EF infrastructure while building docker images encountered problem -i/o timeout. with UDP request and coredns (internal DNS implementation in OKD).
Tests must be done on a more recent version of OKD. 4.10
Actual version 4.9.

=> mid december


## 2 - Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)

### Chat service

start working on implementation of Matrix synapse as server and elementweb as web client.

Board consulting:
https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/

### [oniro] Mirror repositories doesn't work

[[oniro] Mirror repositories doesn't work](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581#note_1051017)

As requested, part of priority.

### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

Comments were in `pending mode` on gitlab.com for 2 weeks. thread validate!
Task: send a email to PMC 

### Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

=> waiting on feedback from Stefan


### Implements pipeline workflow for tagging Oniro project release (IMPORTANT)

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

### Implements job for publishing artifacts to download.eclipse.org (IMPORTANT)

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

Need feedback!

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project


## 3 - Prospects pipeline

* Finish cleanup artifacts on Oniro project.
* Continue on chat service.
* Implements jobs for tagging and publishing to download.eclipse.org.

## 4 - Questions/decisions

* `meta-ledge-secure` and `meta-ts` has been modified according to the proposition.
* Tag process: don't use release branches for trigerring tagging jobs/pipelines.
* Need to investigating on how change annotation tag, by adding/changing signature, for automation.
* Implements: jobs tags with signing and publishing to download.eclipse.org.
* No checksums and artifacts signing needed while publishing to download.eclipse.org.
