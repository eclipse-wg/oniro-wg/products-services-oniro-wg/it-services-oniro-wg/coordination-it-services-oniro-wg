# Oniro IT infrastructure and services coordination meeting 2022wk27

[[_TOC_]]

## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)

## Agenda

* Pipeline migration
* Fossology and to the related migration procedure presentation meeting
* Setup mirror push to gitee.com
 
### Pipeline migration

Start runner implementation in okd environment at EF.
Thanks @Pawel. sent `config.toml` configuration file.

memory and number of cores (cpu limits) configuration work in progress
oom on jobs!

After meeting: 
@pawel ask for access prometheus metrics
```
pastanki[m] 12:15:40
Hi Sebastien
sorry, I forgot about one thing...
Is it possible for me to get access to prometheus statistics from gitlab ?
I'm trying to correlate high load/memory usage with who is running what job
I have only access to current gitlab-runner prometheus
```

### Fossology and to the related migration procedure presentation meeting


Mail Alberto July 1st : https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/blob/main/fossology_migration.md


oniro-compliance-toolchain meeting : July 6th


### Setup mirror push to gitee.com

see: https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1503

We need to maintain a mirror of some of the repositories under eclipse/oniro-core on gitee.com server.  We would therefore like to take advantage of the GitLab support for doing that automatically.  See https://docs.gitlab.com/ee/user/project/repository/mirror/
The repositories that needs to be mirrored are


https://gitlab.eclipse.org/eclipse/oniro-core/oniro -> https://gitee.com/oniroproject/oniro


https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony -> https://gitee.com/oniroproject/meta-openharmony


=> Would you mind explaining the use case for this?




### Action

* Seb H.
    * Give access to Pawel prometheus access 

### Answer

Give access to Pawel prometheus access
=> no prometheus and grafana available.