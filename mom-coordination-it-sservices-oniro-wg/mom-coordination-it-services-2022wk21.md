# Oniro IT infrastructure and services coordination meeting 2022wk21

[[_TOC_]]

## Data

* Schedule:
	* Date: May 21st
	* Time: 12:00 to 12:45 UTC+2 (CEST)
* Chair: Agustin B. B.
* Minutes taker: Agustin B. B.
* Join via Zoom
	* #link <https://eclipse.zoom.us/j/81658946553?pwd=Ni9NdHhZYzFlaW1VV2x6cXAyTmprZz09>
	* Meeting ID: 816 5894 6553
	* Passcode: 536112


## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)
* Agustin B.B. (EF)

## Agenda

* Welcome and introductions 5 min
* Set up the schedule for this meeting series - Agustin 5 min
* Define where we will collaborate: tickets, wiki... - Agustin 5 min
* Which services need to be migrated or implemented from scratch? - Agustin 10 min
* Step 1, prioritization - Agustin 10 min
* AoB - 5 min

### Welcome

First meeting
* Sebastien introduce himself
* Question about the release.
	* Discussion about limitations on our ability to release binaries. We will not for now.
   * Which basic documents should be read to understand how yocto/bitbake works, for somebody that is new to the topic?
* The rest of the participants intro themselves.

### Set up the schedule for this meeting series

Tuesdays at 12 UTC + 2 (CEST)

### Define where we will collaborate: tickets, wiki...

* Chat: #oniroproject libera.chat
* Tickets:
   * Helpdesk to interact with EF IT/release management service 
   * Tickets at the coordination repo of the it-services-oniro-wg gitlab subgroup for other topics. #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg>
* Documentation: wiki #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/it-services-oniro-wg/-/wikis/home>

### Which services need to be migrated or implemented from scratch? 

EF is working on a document to be presented next week as draft for discussion.

### Step 1, prioritization

* Andrei:
   * container registry. Any registry.
   * Sebastien have the permissions oon gitlab that allow him to provide a quicker answer to Oniro devs.
   * ref : [https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1108](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1108)
* Pavel Z.
	* Keysigning for releases.
* Pawel S.
	* Expose some of the gitlab ECA logs, so users have more information to doo debugging. They do not know if the problem is infra or jobs.  
	* does it ref to this issue! : [Support for pushing initial code that doesn't pass CLA git hook] (https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/895)
* Alberto P.
	* FOSSology
	* The pipelines configuration and scancode
