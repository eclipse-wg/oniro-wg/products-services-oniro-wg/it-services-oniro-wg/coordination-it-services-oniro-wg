# Oniro IT infrastructure and services coordination meeting 2022wk50

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Andrei G. (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)

## Agenda

The structure (agenda) :
* Focus EF - Oniro 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## 1 - Focus EF - Oniro

### Commit message with `Related to ...issue`

[After many comment about `related ...` issues format.](https://gitlab.eclipse.org/eclipse/oniro-core/docs/-/merge_requests/36#note_1054086)

Review has to be done on all MR.

[Reorganize commit message with `related to..`in MR.](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/68)

### Chat service status

[Global Board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/)

Accessible address: `https://chat-staging.eclipse.org/`

Short resume: 
* Elementweb and synapse deploy in staging 
* [Loadbalancing and clustering working with implementation of matrix-media-repo](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/23)
* [oidc configuration done on multiple provider: eclipse, github, gmail, gitlab](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/3)
* [Federation](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/28)
* Fonctionnal configuration: 
  * matrix preview
  * elementweb features :
      * "feature_exploring_public_spaces": true,
      * "feature_thread": true,
      * "feature_html_topic": true,
      * "feature_favourite_messages": true,
      * "feature_roomlist_preview_reactions_all": true,
      * "feature_presence_in_room_list": true,
      * "feature_pinning": true,
      * "feature_report_to_moderators": true,
  * ...
* Administration with [synapse-admin](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/22) 

Have to discuss about: 
* [rooms/spaces organization](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/27)
* [Restrict room creation](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/25)

Next step:
* test + configuration
* Deploy multi env

### Cleanup jobs artifacts from Oniro and forks Oniro project

[[oniro] Disable public access for artifacts in the oniro-core group](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2284)

Almost Finish!

### HELPDESK Mirror repositories doesn't work

[[oniro] Mirror repositories doesn't work](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581)

Mirroring in place for [meta-zephyr project](https://gitlab.eclipse.org/eclipse/oniro-core/third-party/mirrors/meta-zephyr)


### DNS issue for resolving artifact from github

Problem still present with version 4.9
Tests must be done on a more recent version of OKD. 4.10


## 2 - Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)

### Chat service

Board consulting:
https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/

### Reorganize commit message with `related to..`in MR. (URGENT)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/68

### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

Comments were in `pending mode` on gitlab.com for 2 weeks. thread validate!
Task: send a email to PMC 

### Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

=> waiting on feedback from Stefan


### Implements pipeline workflow for tagging Oniro project release (IMPORTANT)

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

### Implements job for publishing artifacts to download.eclipse.org (IMPORTANT)

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)


=> 5/8 merge.


### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project

## 3 - Prospects pipeline

* Review commit message in all MR 
* Continue on chat service.

## 4 - Questions/decisions

* create a issue on [matrix preview feature](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/29)
* close mirroring issue, and wait for comment or issue creation about mirroring behavior.
