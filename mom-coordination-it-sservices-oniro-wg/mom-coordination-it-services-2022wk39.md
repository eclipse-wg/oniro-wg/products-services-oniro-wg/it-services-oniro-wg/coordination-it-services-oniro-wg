# Oniro IT infrastructure and services coordination meeting 2022wk39

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Pawel S. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)

## Agenda

* Actions from last week 
* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 

## Migration Pipeline issues 

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/boards


New errors since sync with kirkstone

Globally two problems: 
* Configuring runner to fit job needs in term of consumption (cpu/ram) (most of the time it's SIGTERM/OOM killer)
* Fetcher Error happens randomly (network failure, ...)


### Oniro MIgration: cpio: write failed Resource temporarily unavailable (#31) (OPEN) (IMPORTANT)
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/31

```
create archive failed: cpio: write failed - Resource temporarily unavailable
```

### Oniro MIgration: Bitbake Fetcher Error: git://gitlab.eclipse.org/eclipse/oniro-core/linux.git (#32) (OPEN) (IMPORTANT)
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/32


```
ERROR: Bitbake Fetcher Error: FetchError('Unable to fetch URL from any source.', 'git://gitlab.eclipse.org/eclipse/oniro-core/linux.git;protocol=https;name=machine;branch=oniro/v5.10/base;')
```

### Oniro MIgration: rust-llvm failed with exit code '1' (#33) (OPEN) (IMPORTANT)
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/33

SIGTERM on rust-llvm build.

 ```
 ERROR: Task (virtual:native:/builds/heurtemattes/oniro-migration.tmp/workspace.2SD8aQmppU/oe-core/meta/recipes-devtools/rust/rust-llvm_1.59.0.bb:do_compile) failed with exit code '1'
```

### Oniro migration: uploading artifact context deadline exceeded (timeout) (#30) (OPEN) (CRITICAL)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/30

Maintenance today on EF infra
https://www.eclipsestatus.io/incidents/jcyhwgwyrzgz?u=4h2k0gcs2lnn

As part of https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/30, we will be conducting network maintenance to several compute nodes. During this time, unexpected build failures may occur.



### Oniro Migration: Fetcher failure (#20) (OPEN) (IMPORTANT)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/20

Configure CI_ONIRO_BB_LOCAL_CONF_FETCHCMD_wget with more retry, and github bot authentication.



### Oniro Migration: Global pipeline performance (#27) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/27


### Oniro migration: tmpfs mount for /build (#24) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/24

reopen, it's possible to target specific node in EF infrastructure.
Performance tests are on their way!

### Oniro Migration: initial PVC (#18) (OPEN) (Review) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/18

Provide cache directory structure creation for PVC in gitlab-ci.


### Oniro Migration: error dialing backend: remote error: tls: internal error. (#16) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/16

EF infrastructure pb.
Try workaround with CI_SERVER_TLS_CA_FILE to implements in runner configuration.


### Oniro Migration: host contamination (#21) (OPEN) (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/21

need to test configuration about bitbake warning

### Oniro Migration: bitbake shared-state-cache working and strategy (#29) (OPEN)  (PRIORITY MODERATE-LOW)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/29

Investigation around bitbake cache management:

How it works?
How it grows up?
What is the Oniro team strategy about invalidating cache, ttl, metrics on sizing evolution, ...?

NOTE: Each pipeline build grows cache by 1G of storage.

@Pawel


## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D

### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

No change!!

### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

No change!!


### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

No change!


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602



## Decisions




## Actions

Pawel:
 - bitbake shared-state-cache working and strategy (#29)

