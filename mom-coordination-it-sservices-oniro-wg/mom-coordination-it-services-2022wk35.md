# Oniro IT infrastructure and services coordination meeting 2022wk35

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Pawel S. (Huawei)
* Luca M.
* Zygmunt K. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)


## Agenda

* Request from Oniro team
* Summary of next steps
* Merge trains
* Requests from last meeting
* Pipeline migration
* Artifact target infrastructure
* Gitlab helpdesk title pattern recommendation
* Helpdesk/Oniro issues overview
* Decisions
* Actions
 
## Request from Oniro team

* oniro-compliance-toolchain has been now provisioned. Now it is in incubation state
   * A MR to the IT plan about including this project is expected based on a conversation about this between Agustin and Alberto. The goal is to find out what can be done in the context of the integration and deployment pipelines and what should be done in the context of the IP and license compliance service migration context.
* Prioritization of the existing/openned incidents


## Summary of next steps

* IT PLan: please review it and provide feedback. It will be presented to the Oniro WG SC this coming Thursday.
   * IT PLan document #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md>
* Improvements in communication
   * Additional real time communication is requested. Such communication should lead to tickets but it is needed in order to understand the problem.
   * Agustin/Sebastien will come back with a suggestion since this is not how EF operates. This requires internal discussion, which has started already.
* Integration and deployment pipelines
   * EF will provide an overview of the coming work on this front in the form of a series of tickets and epics. The proposed way in the IT plan is being revisited so it is more consistent with the current tracking system we have at EF (HelpDesk).
   * EF will provide such plan as priority. 

## Merge trains 

Merge trains have been activated in OSTC. Description by Pawel and Zyga of what has been done and the rationale.

Related to gitlab runner performance and big spike in IO wait.

See oniro-dev mailing list: 
https://www.eclipse.org/lists/oniro-dev/msg00089.html

## Requests from last meeting

### gitlab page

Pawel asked if gitlab page is activated. 
Need to see if there is any use case in Oniro project.

Waiting for issue!

Pawel informs that is not needed.

### gitlab runner metrics

Waiting for issue

Pawel informs that is not needed.

### gitlab runner group configuration

Waiting for configuration file.

Discussion about how the runners will look like at EF.

@pawel: Send the latest one of all them.

## Pipeline migration

Oniro project migration:
* fork created in heurtemattes workspace
=> avoid activity pollution in Oniro project.

Oniro migration : (2 blocking point)
* build docker image with kaniko! (root user at EF).
* Failed on "time repo sync --no-clone-bundle" => .workspace  (NFS shared dir permission!).

Build Docker images is now done with buildkit, and a dedicated service hosted at eclipse for building images. 
Kaniko is not supported at EF (need to be root). 

@Pawel
Feedback on pipeline IO Hungry! 

## Artifact target infrastructure

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md#233-download-infrastructure

Many choices are offered.
* download.eclipse.org
* nexus (artifacts are stored in maven format)
* gitlab registry
* build artifacts (current storage choice)

Internal 
download.eclipse.org should be used for high-volume worldwide distribution, as it is backed my mirrors (see: https://wiki.eclipse.org/IT_Infrastructure_Doc#Use_mirror_sites.2Fsee_which_mirrors_are_mirroring_my_files.3F )


Gitlab registry: Container registry for docker images needed
https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1108


Oniro answer: 
All artefacts are only for test.
Only tag will be available in the open.

## Gitlab helpdesk title pattern recommendation

Recommended title format : 
[ONIRO] My problem!

Use releng team board
https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/1899?label_name[]=team%3Areleng

And filter on Oniro pattern for a better view.

With a filter on [ONIRO]
https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/1899?label_name[]=team%3Areleng&search=%5BONIRO%5D


## Helpdesk/Oniro issues overview

### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

* build-dco-check
* build-docker-builder
* build-npm-cspell
* build-reuse

=> don't build, ca-certificate package version doesn't exist anymore in ubuntu repository.


### MR rebase fails occasionally (#1568) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1568

last feedback from Andrei 4 August!
=> need investigation

workaround in place at eclipse: gitlab tuning.

### Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

ongoing discussion!
with Pawel and Denis 

### Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602

ongoing discussion!
with Marta, Mika, Wayne 

### wiki permissions for oniro subproject : meta-oniro-blueprints-flutter (#1612) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1612

Last note from Fred:
Please contact the oniro-blueprints project leads (@pidge, @idlethread, @brgl) to get a committer election started.

### Closed issue

#### please rename https://gitlab.eclipse.org/eclipse/oniro-core/meta-ledge-sesure (#1461) (CLOSE)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1461

Marta ask 2 month ago, resolve 3 weeks ago by fred!

=> take time, triage problem!
no `team:releng` label added

#### GitLab closes issues that shouldn't close based on the default closing pattern (#1585) (CLOSE)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1585

=> closed and as a conclusion: upstream issue.
Should be test again in future gitlab versions!

## Decisions

* No gitlab pages feature
* No gitlab metrics feature (based on gitlab api)
* Artifact target infrastructure is not necessary since in rely only in tag (artefacts are only for test)
  * Let see what web traffic release tag could involved 
* Create board for migration pipeline task

## Actions

Seb: 
  * See logs for helpdesk issue [#1568](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/)
  * Provide epic and roadmap for pipeline migration
  * Enhance helpdesk issue board for Oniro
  * Internal discussion at EF about chat first before creating gitlab issue.
  
Pawel: 
  * Oniro issue [#752](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752)
  * gitlab runner group configuration
  
Oniro team: 
  * Provide feedback on IT plan document
