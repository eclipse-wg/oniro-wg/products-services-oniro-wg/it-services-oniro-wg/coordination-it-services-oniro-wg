# Oniro IT infrastructure and services coordination meeting 2022wk25

[[_TOC_]]

## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)

## Agenda

* Pipeline migration 
* Lava 
* key signing
* license compliance toolchain 


### Pipeline migration 

### Requirements

* No breaking pipeline while migrating
* Performance : almost the same (build time), consumption (cpu, ram, in time)

Questions : 
 * Other requirements to specifify 

### Services migration 

* Gitlab runner implementation at low cost (kubernetes executor) and cache implementation (host cache first)
* Registry (specially docker)
* later optimization :
  * Proxy cache
  * shared cache : object storage!!
  * ...
* Compliance pipeline migration! see how to integrate these project in migration plan.

#### prioritization

Project prioritization for CI migration: 
- HLaas
- eddie
- oniro
- docs
- meta-* (fork project) (meta-openharmony, meta-zephyr, meta-arm)
- blueprint when sources will be available (to reschedule)
  
Add compliance project.

#### Technically

1 - Deactivated runner on HLaas for testing runner.

2 - For other pipeline (proposal) : 

* Create gitlab branch migration per project
* Maybe specify branches by CI feature build
* With the use of runner tags! ex : project `oniro`, `build`, `test`, ...

#### Organizationally

* Plan with project lead (avoid milestone, or disturb merge of new feature)
* Validate with team (workflow, artifacts, build time, ...)

Questions : 
* Who will validate pipeline? 
* Milestone plan by project! ping maintainer

=> create an issue and ping project maintainer to validate pipeline.

#### End pipeline specification with @pawel contribution 

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/questions.adoc

### Lava

Presentation by Stevan S.

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/blob/main/mom-coordination/mom-coordination-20220614-lava-sync.md

After internal discussion, EF will not do anything about LAVA, squadp.
Current configuration by URL, TOKEN in CI variable and external services stay in place. 

### Key signing

Something to do for the release.
Plan a meeting?

Should be prepare for the next milestone in september.
what about IP licence agreement process and signing service

### License compliance toolchain

Topic expected by Oniro team
Meeting 22 June : License-compliance-toolchain migration plan. Preliminary 


### Action

* Seb H.
    * Plan a meeting for signing services
    * see about IP licence agreement process and signing service
* Pawel S.
	* Add contribution to specification

### Answer

Q: see about IP licence agreement process and signing service
No IP license agreement on Alpha release.