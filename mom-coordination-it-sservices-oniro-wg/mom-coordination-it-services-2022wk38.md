# Oniro IT infrastructure and services coordination meeting 2022wk38

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Pawel S. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)

## Agenda

* Actions from last week 
* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 
## Actions from last week 

Pawel:
 - stats about jobs (io utilization/...)

New metrics: 
https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/687#note_1016498

* Jobs running times by job name. Data from gitlab API. That data is contaminated by lack of jobs isolation and long io wait
* Jobs user time by job id. Data from docker cadvisor. Still WiP - correlation to job_names from some other source
* writes bytes. Cadvisor graph
* usage times


## Migration Pipeline issues 

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/boards


### Oniro migration: tmpfs mount for /build (#24) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/24

reopen, it's possible to target specific node in EF infrastructure.
Performance tests are on their way!

### Oniro Migration: Global pipeline performance (#27) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/27


### Oniro migration: uploading artifact context deadline exceeded (timeout) (#30) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/30

EF Infra investigation!

### Oniro Migration: initial PVC (#18) (OPEN) - Review mode

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/18

Provide cache directory structure creation for PVC in gitlab-ci.


### Oniro Migration: Fetcher failure (#20) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/20

Configure CI_ONIRO_BB_LOCAL_CONF_FETCHCMD_wget with more retry, and github bot authentication.



### Oniro Migration: error dialing backend: remote error: tls: internal error. (#16) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/16

EF infrastructure pb.
Try workaround with CI_SERVER_TLS_CA_FILE to implements in runner configuration.


### Oniro Migration: Build-clang stage compilation failed (#26) (CLOSED)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/26

All jobs in `build-clang` stage failed.

Upgrading cpu and especially memory works:
CPU: 12
RAM: 32

### Oniro Migration: host contamination (#21) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/21

need to test configuration about bitbake warning

### Oniro Migration: bitbake shared-state-cache working and strategy (#29) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/29

Investigation around bitbake cache management:

How it works?
How it grows up?
What is the Oniro team strategy about invalidating cache, ttl, metrics on sizing evolution, ...?

NOTE: Each pipeline build grows cache by 1G of storage.

@Pawel

## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D

### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

No change!

### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

Awaiting feedback!


### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

No change!


### [oniro] wiki permissions for oniro subproject : meta-oniro-blueprints-flutter (#1612) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1612

no change!
state:block


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602

first tests from Marta!


## Decisions


## Actions

