# Oniro IT infrastructure and services coordination meeting 2022wk36

[[_TOC_]]


## Participants


* Andrei G. (Huawei)
* Pawel S. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF) 
* Agustin B.B. (EF)

## Agenda

* IT Plan: please review it and provide feedback
* Improvements in communication
* Helpdesk Priority
* Migration Project issues overview
* Artifact target infrastructure
* Helpdesk/Oniro issues overview
* Closed issues
* Decisions
* Actions
 

## IT Plan: please review it and provide feedback

MR from Alberto: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/merge_requests/22

wait for additionnal comment!

We will provide this week about pipeline integration in the IT Plan.


## Improvements in communication

### IT services Mom coordination mailing list
https://www.eclipse.org/lists/oniro-dev/msg00116.html

### Helpdesk board

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?search=%5Boniro%5D

Define priority board in helpdesk
Oniro has to filter by title [Oniro]

Recommended title format for writing issue in helpdesk
[ONIRO] My problem!

Or use use releng team board
https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/1899?label_name[]=team%3Areleng&search=%5Boniro%5D

### it-services-oniro-wg and pipelines-architecture-oniro-wg board

it-services-oniro-wg is a global working board (mainly pipeline migration at this point)
pipelines-architecture-oniro-wg is dedicated to pipeline migration

Pipeline migration board:
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/boards

### Other

Agreement on adding references to all the above links on the wiki and/or IT plan.
* @toscalix created a links and references section on the JOin Us and Contribute [wiki page](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Join_Oniro#links-and-references)


## Helpdesk Priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D


## Migration Project issues overview

### Oniro Migration : Failed on "time repo sync --no-clone-bundle" (#11) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/11

SIGTERM throw to pod

solution: disk space quota was too low

### Oniro Migration : Failed on "bitbake" execution (#12) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/12

same as #11

### Oniro Migration: THREAD Option definition (#13) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/13

BB_NUMBER_PARSE_THREADS
BB_NUMBER_THREADS
PARALLEL_MAKE

Pawel provided gitlab runner definition


### Oniro Migration: do_populate_sysroot failed! (#14) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/14 

Introduce $TMPDIR definition as $CI_PROJECT_DIR.tmp


## Helpdesk/Oniro issues overview


### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

* build-dco-check
* build-docker-builder
* build-npm-cspell
* build-reuse

=> don't build, ca-certificate package version doesn't exist anymore in ubuntu repository.

### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

ongoing discussion!
with Pawel and Denis 

### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602

ongoing discussion!
with Marta, Mika, Wayne 

### [oniro] wiki permissions for oniro subproject : meta-oniro-blueprints-flutter (#1612) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1612

Last note from Fred:
Please contact the oniro-blueprints project leads (@pidge, @idlethread, @brgl) to get a committer election started.


## Closed issues

### [oniro] Add tag to Oniro group runner 308 (#1736) (CLOSE)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1736

add `high_io` label to runner `strong-mustang`


### [oniro] Mail sent by my does not show up in the archive (#1727) (CLOSE)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1727

delay in refreshing archive list

### [oniro] MR rebase fails occasionally (#1568) (CLOSE)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1568

last feedback from Andrei 4 August!
=> need investigation

workaround in place at eclipse: gitlab tuning.


## Decisions

No particular decision.

## Actions

Pawel:
 - stats about jobs (io utilization/...)