# Oniro IT infrastructure and services coordination meeting 2022wk30

[[_TOC_]]

## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)
* Agustin B.B. (EF)

## Agenda

* Pipeline migration
* helpdesk issues
 
### Pipeline migration

first gitlab runner group creation in Oniro.

Test ok on two project : HLAAS and Eddie
Branch are created.

After many test, fork created in heurtemattes workspace.

Oniro migration : (2 blocking point)
* build docker image with kaniko! (root user at EF).
* Failed on "time repo sync --no-clone-bundle" => .workspace  (NFS shared dir permission!).


Feedback on pipeline IO Hungry! @Pawel


### helpdesk 

#### Support for pushing initial oniro-blueprint code that doesn't pass CLA git hook (#1588)

closed 
similar to https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/895
    
### MR rebase fails occasionally (#1568)

no particular feedback since

Update August 4th. 
https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1568#note_958772

I don't think anything got improved sadly. Just now on the eclipse/oniro-core/oniro!246 MR I got the same behaviour:

Activated the "Rebase" button and the UI errored out with "Rebase failed: Rebase locally, resolve all conflicts, then push the branch. Try again."
Click the same button again - same error.
Refresh.
Click the same button - passed all good.

Again, no manual rebases were involved. The MR has no conflicts with the destination branch so GitLab should (and eventually does) be able to handle this directly from the UI.


### Request for resources for discussing security issues/potential IP violations (#1602)

ongoing discussion!

### gitlab page

Pawel ask if gitlab page is activated. 
Need to see if there is any use cased in Oniro project.

### Action

Pawel: 
    * gitlab runner group configuration
    * fill an issue about gitlab runner metrics
    * Fill an issue about gitlab page

Seb: 
    * See logs for #1568