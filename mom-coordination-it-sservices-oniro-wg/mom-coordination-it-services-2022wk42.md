# Oniro IT infrastructure and services coordination meeting 2022wk42

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Agustin B.B. (EF)

## Agenda

* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 
## Oniro migration

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

2 MR: 
* [Oniro Pipeline migration: migration to dockerhub (Ready to merge)](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/339) 
  =>The source branch is 10 commits behind the target branch
* [Migration EF runner (Ready to merge)](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/332)
  =>The source branch is 16 commits behind the target branch

Need approval, Ready to merge!

NOTE: Rollback on npm-cspell.
[docker image npm-cspell: ubuntu 21 mirror no more available](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/833)

[Oniro Migration: bitbake shared-state-cache working and strategy](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/29)

=> back in Todo


## Hlaas migration

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/1)


(Migration EF runner)[https://gitlab.eclipse.org/eclipse/oniro-core/HLaaS/-/merge_requests/39]

Ready to merge!
Approved by Pawel!

## eddie

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/2)

[EF pipeline migration: ci job test failed with](https://gitlab.eclipse.org/eclipse/oniro-core/eddie/-/issues/17)
=> waiting answer

```
    request.path = ".well-known/core";
```

workflow: in progress
ipv6, okd SCC (security context constraint), need investigation.

[EF pipeline migration: ci job eddie-zephir failed failed due to permission denied](https://gitlab.eclipse.org/eclipse/oniro-core/eddie/-/issues/18)

propose MR: https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16
What about provided this project under EF gitlab with dedicated runner. 
I had problems with gitlab and their new policies around gitlab shared runner minutes consumption.
Provide buildkit configuration in gitlab-ci.yml.


## meta-openharmony migration

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/6)


[Migration EF runner (draft))](https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony/-/merge_requests/86)

NOTE: Migration needs to be coordinate. => change CI_REGISTRY_* values
see: [eclipse/oniro-core/meta-openharmony!86](https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony/-/merge_requests/86#note_1026196) 


[meta-openharmony Migration: runner in china area](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/35)

=> any news?

[meta-openharmony migration: SSL verification failure](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/39)

workflow: in progress
No change since last week!

[meta-openharmony migration: Reusing repo mirror failed!](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/46)

workflow: in progress
No change since last week!

## sysota migration

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/4)

[EF pipeline migration: squashfs-tools should be provided by an image](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/25)

=> create a new docker image with squashfs
workflow:in Progress

[EF pipeline migration: Arch target on job go-test](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/24)
workflow:done

[sysota migration: permission denied on moving go cache](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/40)

=>GOPATH: $CI_PROJECT_DIR.tmp/go
workflow:done

[sysota migration: spread-qemu: [impish, ubuntu, 21.10] failed!](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/45)

=>propose to remove ubuntu 21.10 from spread matrix job.
workflow:done

[Sysota migration: cspell new version](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/42)

New spread image in oniro project
=> allow_failure: true
workflow:done

[sysota migration: go-fmt job formatting failed](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/43)

=> format two files for go fmt.
workflow:done

[EF pipeline migration: Arch target on job cross-build](https://gitlab.eclipse.org/eclipse/oniro-core/sysota/-/issues/26)
=> missreading
workflow:done


## meta-zephyr migration

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/7)

Bloc by oniro migration

```
    - '/.oniro-ci/dco.yaml'
    - '/.oniro-ci/reuse.yaml'
    - '/.oniro-ci/build-generic.yaml'
    - '/.oniro-ci/test-generic.yaml'
    - '/.oniro-ci/machines-and-flavours.yaml'

```

=> will be test with specific branch if it take too logn to merge

## docs

[Top issue](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/5)

Bloc by oniro migration

```
   file:
    - .oniro-ci/dco.yaml
    - .oniro-ci/reuse.yaml
```

=> will be test with specific branch if it take too long to merge

## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D

Regression on the last gitlab version 15 on board filter.


### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

Awaiting feedback! For 1 month

### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

No change! For 1 month


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602

No change! For 1 month


## Decisions


Change meeting plan perperstive following the one provided by the working group.
The structure (agenda) :
* Ef-focus - Oniro PgM 5 min
* Priority prospects - all 5 min
* Prospects pipeline - all 10 min
* Next Iteration targets - all 10 min
* AOB 5 min


## Actions

