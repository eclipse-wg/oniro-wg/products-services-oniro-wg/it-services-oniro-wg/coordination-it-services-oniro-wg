# Oniro IT infrastructure and services coordination meeting 2022wk51

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Andrei G. (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)
* Agustin B.B. (EF)

## Agenda

The structure (agenda) :
* Focus EF - Oniro 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## 1 - Focus EF - Oniro

### Pipeline migration

First EF runner migration on HLaas project: 
[HLaas migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/1)

### Chat service status

[Global Board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/)

Accessible address: `https://chat-staging.eclipse.org/`

Short resume: 
* Elementweb and synapse deploy in staging 
* [Authentication configuration](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/3)
  * Auth only with EF account
* [Federation](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/28)
* Fonctionnal configuration: 
  * [Synapse feature activation: search for space](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/element-web/-/issues/12)

Specifics configuration: 
* [rooms/spaces organization](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/27)
  * Decide internally to let project organize themself first on demand via helpdesk issue.
  * Dedicated issue for Oniro Organization: [Chat service room/space organization](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/69)

* [Restrict room creation](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/25), Two modules were created:
  * [synapse_user_control](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/blob/main/modules/synapse_user_control/module.py): control public room creation
  * [synapse_prevent_encrypt_room](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/blob/main/modules/synapse_prevent_encrypt_room/module.py): control public room encryption

Looking for the first beta-testers!

### Cleanup jobs artifacts from Oniro and forks Oniro project

[[oniro] Disable public access for artifacts in the oniro-core group](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2284)

Done!

### DNS issue for resolving artifact from github

Tests done on a more recent version of OKD. 4.10
No problem found!
OKD migration in january.

NOTE: have an impact on chat-service [Error in preview url from github](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/32)

## 2 - Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)

### Chat service

Board consulting:
https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/

### Reorganize commit message with `related to..`in MR. (URGENT) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/68

State: Closed with reword commits and rebase all MR 

### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

Comments were in `pending mode` on gitlab.com for 2 weeks. thread validate!
Task: send a email to PMC 

State: no change

### Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

State: waiting on feedback from Stefan

### Chat service room/space organization (IMPORTANT)

[Chat service room/space organization](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/69)

State: new issue

### Implements pipeline workflow for tagging Oniro project release (IMPORTANT)

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

State: no change

### Implements job for publishing artifacts to download.eclipse.org (IMPORTANT)

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

State: no change

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

State: No change, 5/8 merge.

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project

State: no change

## 3 - Prospects pipeline

* Continue on chat service.

## 4 - Questions/decisions
