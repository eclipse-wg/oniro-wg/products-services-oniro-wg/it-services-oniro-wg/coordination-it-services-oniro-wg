# Oniro IT infrastructure and services coordination meeting 2023wk01

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF)
* Agustin B.B. (EF)

## Agenda

The structure (agenda) :
* Focus EF - Oniro 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## 1 - Focus EF - Oniro

### Pipeline migration


[ feat: add eca check. ](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/360)

=> Added to merge train but failed! (conflicting requests! in logs)

[ Oniro Pipeline migration: Migration EF runner ](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/332)

=> pipeline failed! on linux-seco-*-s job with package `harfbuzz_4.0.1.bb`


### Chat service status

Some news from Matrix foundation: https://matrix.org/blog/2022/12/25/the-matrix-holiday-update-2022

> In short: folks love the amazing decentralised encrypted comms utopia of Matrix. But organisations also love that they can use it without having to pay anyone to develop or maintain it. This is completely unsustainable, and Element is now literally unable to fund the entirety of the Matrix Foundation on behalf of everyone else - and has had to lay off some of the folks working on the core team as a result.


## 2 - Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)

### Chat service (URGENT)

Board consulting:
https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/


### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.
Task: send a email to PMC

State: no change

### Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

State: waiting on feedback from Stefan

### Chat service room/space organization (IMPORTANT)

[Chat service room/space organization](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/69)

Comment from Agustin:

>Before starting to propose something for Oniro only, let me confirm there is no intention by EMO and/or WG team at EF to pre-define some channels.


### Implements pipeline workflow for tagging Oniro project release (IMPORTANT)

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

State: no change

### Implements job for publishing artifacts to download.eclipse.org (IMPORTANT)

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

State: no change

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

State: No change, 5/8 merge.

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project

State: no change

## 3 - Prospects pipeline

* Continue on chat service.
* Debug Oniro pipelines

## 4 - Questions/decisions
