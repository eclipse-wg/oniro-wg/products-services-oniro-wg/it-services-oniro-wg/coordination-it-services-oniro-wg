# Oniro IT infrastructure and services coordination meeting 2022wk26

[[_TOC_]]

## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)
* Agustin B.B. (EF)

## Agenda

* IP License toolchain presentation meeting

## IP License toolchain presentation meeting

* When is the best timing for the Oniro IP compliance team to migrate the existing service and tooling?
Alberto : no particular urgency
migration seemlessly as possiblity, should avoid migration, because team will be in a rush
dedicate at least a week, for ressource to be concentrated with migration

Peter dedicated next week for this. pipeline and tools

* Schedule the meeting series to learn about the existing infrastructure and services.
     * It has to be in ET compatible time zone when possible.   

* Go over the current document plan to polish the current text there.
    

learn about ip toolchain: 
dedicated more than half a hour during july


https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/blob/main/fossology_migration.md



### Action

* Alberto Pianon 
    * Documentation
* Agustin : 
    * oniro-compliance-toolchain meeting


### Answer

oniro-compliance-toolchain meeting : July 6th
	
•	agustin.benito@eclipse-foundation.org - creator
•	pianon@array.eu
•	sebastien.heurtematte@eclipse-foundation.org
•	pawel.stankiewicz@gmail.com
•	andrei.gherzan@huawei.com
•	pavel.zhukov@huawei.com - optional


Mail Alberto July 1st : https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/blob/main/fossology_migration.md



we have prepared an (hopefully) brief but comprehensive introduction to Fossology and to the related migration procedure, aimed at sysadmin(s) that need to handle the migration process and start administrating the migrated Fossology instance.

https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/blob/main/fossology_migration.md

Please note that we have a custom Fossology installation, with some tweaks and fixes applied in order to integrate Fossology in our toolchain (some fixes have already been applied upstream, others are ongoing but pending).

Following the learning-by-doing principle, we propose that EF sysadmin(s) start installing, configuring and testing a fresh Fossology instance in EF infrastructure, following the installation and test procedures suggested in the guide - so while testing they may start getting acquainted with Fossology and learn how it is used in the context of Oniro. Such instance may be then used for the migration process (test data will be deleted by the migration).

As to data migration, we propose a multi-step/delta migration (details in the guide), which should minimize service disruption for the Audit Team that works on Fossology.

Feel free to provide any feedback directly in the gitlab repo above (or to even try starting the installation process in the EF infrastructure, so you may provide even more sensible feedback).