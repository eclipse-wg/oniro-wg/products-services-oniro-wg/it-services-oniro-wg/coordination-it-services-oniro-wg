# Oniro IT infrastructure and services coordination meeting 2022wk46

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)
* Agustin B.B. (EF)

## Agenda

The structure (agenda) :
* Ef-focus - Oniro PgM 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


# Ef-focus - Oniro

## ECA Check

No history in email account for ECA. 
ECA API need: commiter, author, and parent commit.

Internal discusion about email change over time. (kepp history)


## commit convention to ci

Apply to all MR.

```
ci: ...
```

## Oniro project website

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

=> need to define structure organization
|
|- index.html -> redirect to https://projects.eclipse.org/projects/oniro.oniro-core
|- /eddie -> subfolder containing : https://gitlab.eclipse.org/eclipse/oniro-core/eddie-website

NOTE: prerequisite for eddie-website documentation hosting

## oniro compliance toolchain - Starting migrating 

Need to define tasks and priorities!
Meeting with Alberto Pianno Nov 15.

Propose this priority: 
* tinfoilhat
* aliens4friends
* pipelines
* dashboard
* docs

.gitlab-ci.yml file has been updated with `gitlab-ci-templates`, including jobs: 
* reuse
* dco
* buildkit
* hadolint
* ECA check

Wait on dockerhub account creation: onirocompliancetoolchain username.


## Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)


### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

Comments were in `pending mode` on gitlab.com for 2 weeks. thread validate!
Task: send a email to PMC 


### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

Need feedback!

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project


## Prospects pipeline

### Migration status

* Oniro: 2 MR to validate!
* HLaas: 1 MR to validate!
* meta-openharmony : 1 MR to validate 
* meta-zephyr: MR ready + Prerequisite Oniro MR validation
* docs: MR ready + Prerequisite Oniro MR validation 
* Sysota: 1 MR to validate!
* eddie: Waiting on zephyr MR validation for eddie. + website publication

### Compliance toolchain pipeline migration 

Project priority to define

## Questions/decisions



