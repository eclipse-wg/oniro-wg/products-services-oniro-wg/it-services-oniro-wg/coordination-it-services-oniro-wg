# Oniro IT infrastructure and services coordination meeting 2022wk29

[[_TOC_]]

## Participants

* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)

## Agenda

* Pipeline migration
* group runner for oniro blueprint
* chat service
* Setup mirror push to gitee.com
 
### Pipeline migration

memory and number of cores (cpu limits) configuration work in progress
oom on jobs!

=> Any news about tuning

send new gitlab runner configuration. 
project oniro-core.
connect with the same gitlab runner server to oniro-blueprint.

IO hungry
try tmpfs


After meeting: 
@pawel ask for access prometheus metrics
```
pastanki[m] 12:15:40
Hi Sebastien
sorry, I forgot about one thing...
Is it possible for me to get access to prometheus statistics from gitlab ?
I'm trying to correlate high load/memory usage with who is running what job
I have only access to current gitlab-runner prometheus
```

=> Wait on an issue creation.
=> No metrics on gitlab/runner available, grafana is not activated.


Creation of an internal project as JIRO project: 
gitlab runner as code : with definition of ressources pack.
First tests with Oniro projects will begin at the end of the week!

### group runner


### Chat service
=> Wait on an issue creation.
=> No metrics on gitlab/runner available, grafana is not activated.

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1558

Plan :
* first beta version Sept 19 for project oniro  
* final version for the community : Oct 03


No chat migration from other tools!


# Setup mirror push to gitee.com

see: https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1503

We need to maintain a mirror of some of the repositories under eclipse/oniro-core on gitee.com server.  We would therefore like to take advantage of the GitLab support for doing that automatically.  See https://docs.gitlab.com/ee/user/project/repository/mirror/
The repositories that needs to be mirrored are

https://gitlab.eclipse.org/eclipse/oniro-core/oniro -> https://gitee.com/oniroproject/oniro


https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony -> https://gitee.com/oniroproject/meta-openharmony


=> Would you mind explaining the use case for this?
=> Any news?


Information till the end of tomorrow.



### Action

* Seb H.
