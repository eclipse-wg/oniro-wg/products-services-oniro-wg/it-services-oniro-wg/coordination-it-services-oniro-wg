# Oniro IT infrastructure and services coordination meeting 2022wk37

[[_TOC_]]


## Participants

* Andrei G. (Huawei)
* Zygmunt K. (Huawei)
* Pawel S. (Huawei)
* Sebastien H. (EF)
* Fred G. (EF) 

## Agenda

* Actions from last week 
* IT Plan: please review it and provide feedback
* Improvements in communication feedback
* Migration Pipeline issues 
* Helpdesk/Oniro issues 
* Decisions
* Actions
 
## Actions from last week 

Pawel:
 - stats about jobs (io utilization/...)

=> https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/764#note_981794
  
Oniro Migration: runner specification strong-mustang and knowing-macaque (#22) (OPEN)
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/22


## IT Plan: please review it and provide feedback

Response from Fred on Alberto's MR: 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/merge_requests/22#note_1012557

@alpianon Can you comment on the scope of both the standard pipelines and the custom pipelines for the Oniro-compliance-toolchain? How many pipelines need to be migrated?

## Improvements in communication feedback

Any comments?

## Migration Pipeline issues 

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/boards


### Oniro migration: tmpfs mount for /build (#24) (CLOSE)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/24

Abandon solution for the moment!
Can't fit Oniro first build (cache provisionning) 


### Oniro Migration: initial PVC (#18) (OPEN) - Review mode

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/18

Provide cache directory structure creation for PVC in gitlab-ci.

### Oniro Migration: hang problem (#17) (REOPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/17

Issue reopen after changed PersistentVolumeClaim configuration

Need to find a failfast option for this issue. Because jobs stay stuck for a very long time before failing. They reach the timeout which is 6h.

### Oniro Migration: Fetcher failure (#20) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/20

Configure CI_ONIRO_BB_LOCAL_CONF_FETCHCMD_wget with more retry
Need to create a github bot.


### Oniro Migration: error dialing backend: remote error: tls: internal error. (#16) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/16

EF infrastructure pb.
Try workaround with CI_SERVER_TLS_CA_FILE to implements in runner configuration.


### Oniro Migration: build Performance (#25) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/25

Continue perf test.

### Oniro Migration: Build-clang stage compilation failed (#26) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/26

All jobs in `build-clang` stage failed.

Logs about the origin not clear!

### Oniro Migration: host contamination (#21) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/21

need to test configuration about bitbake warning


### Oniro Migration: runner specification strong-mustang and knowing-macaque (#22) (OPEN)

https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/22

usefull information about Oniro runner in OSTC

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/764#note_981794


## Helpdesk/Oniro issues 

### Review priority

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/boards/2465?label_name[]=team%3Areleng&search=%5Boniro%5D

### Kirkstone Images build failed with bad ca-certificate version (#752) (OPEN)

https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/752

Always Open!

### [oniro] Location for Doxygen documentation for Eddie project (#1923) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

Needs new location before 30th of Sep.


### [oniro] Mirror repositories doesn't work (#1581) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1581

ongoing discussion!
with Pawel and Denis 


### [oniro] wiki permissions for oniro subproject : meta-oniro-blueprints-flutter (#1612) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1612

Last note from Fred 1 week:

State: block, awaiting feedback!


### [oniro] Request for resources for discussing security issues/potential IP violations (#1602) (OPEN)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602

ongoing discussion!
with Marta, Mika, Wayne 

=> state review, awaiting feedback!


## Decisions


## Actions

