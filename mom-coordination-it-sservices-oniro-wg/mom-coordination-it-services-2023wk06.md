# Oniro IT infrastructure and services coordination meeting 2023wk06

[[_TOC_]]


## Participants

* Luca M. (NOI Techpark)
* Sebastien H. (EF)

## Agenda

The structure (agenda) :
* Focus EF - Oniro 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


## 1 - Focus EF - Oniro

### Pipeline migration

[Oniro Pipeline migration: Migration EF runner](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/332)
=> ready to merge


[Clean up the shared-state-cache](https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/merge_requests/391)
=> New MR according to Stefan comment, moving form Oniro MR to this specific one.


### Chat service status

`Infrastructure as code` writing task is finished. Deployment and tests in progress in all environments.
=> done and deploy in all environment.

[Matrix server notification](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/43)
[Agreement definition](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/13)
=> process ok, waiting on internal discussion on how to structure the page.

Working on issue about [media and federated account](https://gitlab.eclipse.org/eclipsefdn/it/releng/chat-service/synapse/-/issues/36)
=> still open, waiting on new dns definition in order to debug.

## 2 - Priority prospects

### Boards 

Chat-service:
* [Workflow board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/)
* [Priority board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/2687)

Pipelines:
* [Workflow board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/2466)
* [Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)


### Oniro project website (URGENT)

Top level project for hosting project website source

[Oniro project website](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/60)

[Location for Doxygen documentation for Eddie project](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923)

State: Agustin will talk to the next Oniro Engineering Weekly meeting the 9th February about website organization.


### Implements pipeline workflow for tagging Oniro project release (IMPORTANT)

[Implements pipeline workflow for tagging Oniro project release](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/66)

State: no change

### Implements job for publishing artifacts to download.eclipse.org (IMPORTANT)

[Implements job for publishing artifacts to download.eclipse.org](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/67)

State: no change

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

State: One more, 7/8 merge. only sysota remains.

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project

State: no change

## 3 - Prospects pipeline

* Chat service: Continue working on legal, and start GDPR investigation.
* Pipeline: in parallel if Oniro pipeline is merged, apply changes to dependent pipelines.

## 4 - Questions/decisions

