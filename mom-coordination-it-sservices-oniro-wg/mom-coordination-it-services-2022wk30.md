# Oniro IT infrastructure and services coordination meeting 2022wk30

[[_TOC_]]

## Participants

* Andrei G. (Huawei)
* Pavel Z. (Huawei)
* Pavel S. (Huawei)
* Alberto P. (Array)
* Sebastien H. (EF)
* Agustin B.B. (EF)

## Agenda

* Pipeline migration
* helpdesk issues
 
### Pipeline migration

Feedback on pipeline IO Hungry! @Pawel

Still on the process configuration of pipeline, gitlab runner performance and availability
smaller instance runner dedicated
token group receive on friday

Gitlab runner as code: 
* Runner project is over. 
* Work on ressource package by group.
* Apply after to Oniro Project HLaas.

first gitlab runner group creation will be done july 16 afternoon.

### helpdesk 

#### Setup mirror push to gitee.com #1503

Wait on information.
Issue was closed.

#### MR rebase fails occasionally #1568

Gitlab configuration change. 
wait till tomorrow if it's change behavior.

#### ECA being enforced for infrastructure-managed repositories #1565

GItlab bot oniro-core-bot@eclipse.org
=> closed

#### Migrate oniro-core excluded forks to their own group #1532

mix two subject.#1565
can we close?

#### Mirror repositories doesn't work #1581

internal sync

#### GitLab closes issues that shouldn't close based on the default closing pattern #1585

Wait on internal configuration: gitlab.rb
can be disable at project level.

### Action

Pawel: 
    * gitlab runner group configuration
    * fill an issue about gitlab runner metrics