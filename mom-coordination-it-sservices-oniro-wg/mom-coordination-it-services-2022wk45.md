# Oniro IT infrastructure and services coordination meeting 2022wk45

[[_TOC_]]


## Participants

* Alberto P. (Array)
* Ettore C. (SECO)
* Andrei G. (Huawei)
* Stefan Schmidt (Huawei)
* Luca M. (NOI Techpark)
* Sebastien H. (EF)
* Fred G. (EF)
* Agustin B.B. (EF)
* Denis R. (EF)

## Agenda

The structure (agenda) :
* Ef-focus - Oniro PgM 10 min
* Priority prospects - all 10 min
* Prospects pipeline - all 5 min
* Questions 5 min


# Ef-focus - Oniro

## Better MR review 

All commits from MR has been reorganizes to facilitate review. 
Most commits have a reference to an issue from project: https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/issues

Exceptions: 
* when changing runner tags 
* minor fix

## Qemu Dedicated VM

For security reason: using dedicated VM to QEMU job test with shell executor, instead of using okd infrastructure with kubernetes executor.

See [Answer from Mikaël Barbero](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/47#note_1035915)

Reference: [Sysota migration: qemu:ubuntu-22.04-64 connection refused](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/47)


## Faster feedback on ECA failures

[Issue Reference](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

[Eca check Job reference](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-ci-templates/-/blob/main/jobs/eca.gitlab-ci.yml)

6 dedicated MR ready to merge for projects except sysota.

## Having an easy way to filter for Oniro issues in the helpdesk repository

Since gitlab 15.2, search issue is not anymore based on sql LIKE/ILIKE operators and has been replaced by text full search postgres feature.
https://gitlab.com/gitlab-org/gitlab/-/merge_requests/90768

Need to find a workaround!

## Refactor pipeline jobs

Refactor jobs in project [gitlab-ci-templates](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-ci-templates)
* reuse
* dco
* buildkit
* hadolint
* ECA check

/!\ Not use in Oniro project!

Proposition to refactor pipelines in a second step when first migration pipeline MRs! will be validated.


## Priority prospects

[Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)

### zephyr-ci-pipelines MR validation (URGENT)

[zephyr-ci-pipelines project in EF gitlab](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50)

Waiting on [MR validation](https://gitlab.com/zygoon/zephyr-ci-pipelines/-/merge_requests/16)

Impact on Eddie project migration.

### meta-openharmony: SOURCE_MIRROR_URL_CH (URGENT)

[meta-openharmony: SOURCE_MIRROR_URL_CH point to china IP Huawei Cloud Service data center](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/48)

waiting on Answer!

Impact on meta-openharmony project migration.

### Faster feedback on ECA failures (IMPORTANT)

[Faster feedback on ECA failures](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/55)

Need feedback!

### Oniro Migration (IMPORTANT)

[Oniro Migration](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/3)

Dependencies for other migration project


### [oniro] Location for Doxygen documentation for Eddie project (HELPDESK)

https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923

see [comment from Matt](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1923#note_1036248)


## Prospects pipeline

### Migration status

* Oniro: 2 MR to validate!
* HLaas: 1 MR to validate!
* meta-openharmony : MR ready + wait on issue:  SOURCE_MIRROR_URL_CH point to china IP Huawei Cloud Service data center
* meta-zephyr: MR ready + Prerequisite Oniro MR validation
* docs: MR ready + Prerequisite Oniro MR validation 
* Sysota: migration with qemu job
* eddie: Waiting on zephyr MR validation for eddie. + website publication

### Compliance toolchain pipeline migration 

Which project pipeline? priority?

## Questions/decisions

* Compliance toolchain pipeline migration: Which project pipeline? priority?
See with Alberto.

* Change commit convention to ci.
https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/commit/8a6bb0bb1d19d56a56dd374c65c754a7dea88b67
=> ci: ...

* How merge commit are handle by ECA check?
See with webdev team.

* Refactor pipeline jobs
  => step 2, after the migration..

* zephyr-ci-pipelines MR Validation.
send a email to PMC: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/pipelines-architecture-oniro-wg/-/issues/50

* Eddie project documentation organization.
Can we organize with sub context: like websites.eclipseprojects.io/oniro.oniro-core/eddie?

